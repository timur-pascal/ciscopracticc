#include <iostream>
using namespace std;

int main() {
  int code = 0, value;

  cout << "Input number => ";
  cin >> value;

  for (int i = 32; i > 0; i--){
    if ((1 << i) & value){
      code += 1;
    }
  }

  cout << code << endl;
  return 0;
}
