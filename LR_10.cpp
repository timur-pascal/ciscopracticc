#include <iostream>
using namespace std;

int main() {
  float grossprice, taxrate, netprice, taxvalue;
  cout << "Input a gross price => "; cin >> grossprice;
  cout << "Input a tax rate => "; cin >> taxrate;

  taxvalue = (grossprice / (taxrate + 100)) * 100 ;
  netprice = grossprice - taxvalue;

  cout << "Next price => " << taxvalue;
  cout << "\nTax value => " << netprice << endl;
  return 0;
}
