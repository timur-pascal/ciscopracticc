#include <iostream>
using namespace std;

int main() {
  int year, a, b, c, d, e;
  cout << "Input year =>"; cin >> year;

  // some strange calculations ?! Gauss are you crazy?
  a = year % 19;
  b = year % 4;
  c = year % 7;
  d = ((a * 19) + 24) % 30;
  e = (2 * b + 4 * c + 6 * d + 5) % 7;
  // print result
  d + e < 10 ? cout << d + e + 22 << " March\n" : cout << d + e - 9 << " April\n";
  
  return 0;
}
