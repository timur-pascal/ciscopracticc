#include <iostream>
#include <iomanip>
using namespace std;
int main() {
	double overall = 0;
	int countCurses;
	cin >> countCurses;
	int **arr = new int *[countCurses];

	for(int i = 0; i < countCurses; i++) {
		int countValues;
		cin >> countValues;
		arr[i] = new int[countValues + 1];
		arr[i][0] = countValues;
		for(int j = 1; j < countValues + 1; j++) {
			int temp;
			cin >> temp;
			arr[i][j] = temp;
		}
	}

	cout << endl << fixed;
	cout.precision(2);
	
	for(int i = 0; i < countCurses; i++) {
		double final = 0;
		cout << "Course " << i + 1 << ": final ";
		for(int j = 1; j < arr[i][0] + 1; j++)
		final += arr[i][j];
		overall += final / arr[i][0];
		cout << final / arr[i][0] << ", grades: ";
		for(int j = 1; j < arr[i][0] + 1; j++){
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
	cout << "Overall final " << overall / countCurses << endl;
	return 0;
}
