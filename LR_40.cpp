#include <iostream>
#include <cmath>
using namespace std;
bool isPrime(int num) {
	bool is = true;
	for (int n = 2; n <= sqrt(num); n++) {
		if (num % n==0) {
			is = false;
			break;
		}
	}
	return (num <= 1) ? !is : is;
}
int main() {
  for(int i = 0; i <= 21; i++) {
    if(isPrime(i)) {
      cout << i << " ";
    }
  }
  cout << endl;
  return 0;
}
