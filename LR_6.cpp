#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main() {
  const float one = 2.3, two = 2.3, tree = 2.123456, four = 2.123456, five = 2.123456;

  cout << one << endl;
  cout << fixed << setprecision(2)  << two << endl;
  cout << setprecision(5) << tree << endl;
  cout << setprecision(2) << round(four * 100) / 100 << endl;
  cout << setprecision(0) << round(five * 1) / 1 << endl;

  return 0;
}
