#include <iostream>
using namespace std;

int main() {
  int sys;
  float value;

  cout << "What number system to use?\nInput 0 for metric our 1 for imperial => "; cin >> sys;

  switch (sys) {
    case 0: cout << "Input value => "; cin >> value;
            value *= 39.37008;
            cout << int(value) % 12 << "'" << value / 12 << "\"\n";
            break;
    case 1: cout << "Input what system to convert?\n3 - meters 0 - either => "; cin >> sys;
            switch (sys) {
              case 0: cout << "Value => "; cin >> value;
                      value *= 0.0254;
                      cout << value << "m\n";
                      break;
              case 3: cout << "Value => "; cin >> value;
                      value *= 0.3048;
                      cout << int(value) << "m\n";
                      break;
              default: cout <<"Wrong input :(\n";
            }
            break;
    default: cout <<"Wrong input :(\n";
  }

  return 0;
}
