#include <iostream>
using namespace std;

int main() {
  float a, b;
  cout << "Input two number => "; cin >> a >> b;
  b = 1 / b;
  a = 1 / a;

  if (a == b){
    cout << "Result are equal (by 0.000001 epsilon)\n";
  } else {
    cout << "Result are not equal (by 0.000001 epsilon)\n";
  }
  
  return 0;
}
